clear
close all
imtool close all
clc

I1 = imread('7.jpg');
subplot(2, 2, 1), imshow(I1);

I1 = rgb2gray(I1);
I1 = medfilt2(I1, [10 10]);
subplot(2, 2, 2), imshow(I1);

I1 = imadjust(I1, [150 240] / 255, [], 0.5);
I1 = imopen(I1, strel('disk', 10));
subplot(2, 2, 3), imshow(I1);

level = graythresh(I1);
I1 = im2bw(I1, level);
I1 = 1 - I1;

I1 = bwmorph(I1, 'erode', 4);
I1 = bwmorph(I1, 'dilate', 15);
subplot(2, 2, 4), imshow(I1);

[labeled, numberObjects] = bwlabel(I1, 8);
numberObjects