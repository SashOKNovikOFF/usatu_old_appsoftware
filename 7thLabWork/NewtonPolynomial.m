function [y] = NewtonPolynomial(x, xPoints, yPoints)

y = yPoints(1);
step = xPoints(2) - xPoints(1);
    
for i = 2:1:length(xPoints)
    xCoeff    = 1;
    diffCoeff = 0;

    for j = 1:1:(i - 1)
        xCoeff = xCoeff * (x - xPoints(j));
    end

    for j = 1:1:i
        diffCoeff = diffCoeff + (- 1)^(j - 1) * nchoosek(i - 1, j - 1) * yPoints(j);
    end
    diffCoeff = diffCoeff / factorial(i - 1) / (- step)^(i - 1);

    y = y + xCoeff * diffCoeff;
end

end