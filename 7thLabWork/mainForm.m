function varargout = mainForm(varargin)
% MAINFORM MATLAB code for mainForm.fig
%      MAINFORM, by itself, creates a new MAINFORM or raises the existing
%      singleton*.
%
%      H = MAINFORM returns the handle to a new MAINFORM or the handle to
%      the existing singleton*.
%
%      MAINFORM('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MAINFORM.M with the given input arguments.
%
%      MAINFORM('Property','Value',...) creates a new MAINFORM or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before mainForm_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to mainForm_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help mainForm

% Last Modified by GUIDE v2.5 23-Dec-2015 00:12:56

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @mainForm_OpeningFcn, ...
                   'gui_OutputFcn',  @mainForm_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before mainForm is made visible.
function mainForm_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to mainForm (see VARARGIN)

% Choose default command line output for mainForm
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes mainForm wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = mainForm_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in plotNewton.
function plotNewton_Callback(hObject, eventdata, handles)
% hObject    handle to plotNewton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

try
    xPoints = eval(get(handles.xInput, 'String'));
    yPoints = eval(get(handles.yInput, 'String'));
    
    if length(xPoints) ~= length(yPoints)
        set(handles.plotNewton, 'String', 'Incorrect input')
    elseif length(xPoints) < 2    
        set(handles.plotNewton, 'String', 'Not enough points')
    else
        set(handles.plotNewton, 'String', 'Newton Polynomial')
        
        xLeftBorder  = xPoints(1);
        xRightBorder = xPoints(length(xPoints));
        nodes = 100;
        step = (xRightBorder - xLeftBorder) / nodes;

        xP = xLeftBorder:step:xRightBorder;
        yP = zeros(1, nodes);
        i = 1;

        for x = xP
            yP(i) = NewtonPolynomial(x, xPoints, yPoints);
            i = i + 1;
        end

        plot(xP, yP, 'r')
        hold on
        plot(xPoints, yPoints, 'g*')
        return
    end
    
    uicontrol(hObject)
catch
    set(handles.plotNewton, 'String', 'Cannot plot')
    uicontrol(hObject)
end

function xInput_Callback(hObject, eventdata, handles)
% hObject    handle to xInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of xInput as text
%        str2double(get(hObject,'String')) returns contents of xInput as a double


% --- Executes during object creation, after setting all properties.
function xInput_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function yInput_Callback(hObject, eventdata, handles)
% hObject    handle to yInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of yInput as text
%        str2double(get(hObject,'String')) returns contents of yInput as a double


% --- Executes during object creation, after setting all properties.
function yInput_CreateFcn(hObject, eventdata, handles)
% hObject    handle to yInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in plotLagrange.
function plotLagrange_Callback(hObject, eventdata, handles)
% hObject    handle to plotLagrange (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
try
    xPoints = eval(get(handles.xInput, 'String'));
    yPoints = eval(get(handles.yInput, 'String'));
    
    if length(xPoints) ~= length(yPoints)
        set(handles.plotLagrange, 'String', 'Incorrect input')
    elseif length(xPoints) < 2    
        set(handles.plotLagrange, 'String', 'Not enough points')
    else
        set(handles.plotLagrange, 'String', 'Newton Polynomial')
        
        xLeftBorder  = xPoints(1);
        xRightBorder = xPoints(length(xPoints));
        nodes = 25;
        step = (xRightBorder - xLeftBorder) / nodes;

        xP = xLeftBorder:step:xRightBorder;
        yP = zeros(1, nodes);
        i = 1;

        for x = xP
            yP(i) = LagrangePolynomial(x, xPoints, yPoints);
            i = i + 1;
        end

        plot(xP, yP, 'bo')
        hold on
        plot(xPoints, yPoints, 'g*')
        return
    end
    
    uicontrol(hObject)
catch
    set(handles.plotLagrange, 'String', 'Cannot plot')
    uicontrol(hObject)
end
