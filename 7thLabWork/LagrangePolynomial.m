function [y] = LagrangePolynomial(x, xPoints, yPoints)

LagrangeCoeff = ones(1, length(xPoints));

for i = 1:1:length(xPoints)
    for j = 1:1:length(xPoints)
        if i ~= j
            LagrangeCoeff(i) = LagrangeCoeff(i) * (x - xPoints(j)) / (xPoints(i) - xPoints(j));
        end
    end
end

y = 0;
for i = 1:1:length(xPoints)
    y = y + yPoints(i) * LagrangeCoeff(i);
end

end