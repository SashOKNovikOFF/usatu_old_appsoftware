clear
clc

%[x, y] = meshgrid(-5:0.01:5, -5:0.01:5);
%ezsurf('0.25 * (x.^2 + y.^2)')
%axis auto

[x, y, z] = meshgrid(-5:0.1:5, -5:0.1:5, -10:0.1:10);
funcXYZ = x.^2 + y.^2 - 4 * z;

colormap hsv
h = slice(x, y, z, funcXYZ, 0, 0, 0)

set(h,'edgecolor','none')