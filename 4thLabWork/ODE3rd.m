clear
clc

analyticFunc = dsolve('Dy + 4 * x^2 * y = 4 * (x^2 + 1) * exp(-4 * x) * y^2', 'y(0) == 1', 'x')

x1 = 0:0.01:0.5;
y1 = subs(analyticFunc, x1);

func = @(x, y) 4 .* (x.^2 + 1) .* exp(-4 .* x) .* y.^2 - 4 .* x.^2 .* y;

parts = 100;
leftBorder  = 0;
rightBorder = 0.5;
step = (rightBorder - leftBorder) / parts;

x2 = leftBorder : step : rightBorder;
y2 = [1];

for i = 2:numel(x2)
    temp = y2(i - 1);
    y2   = [ y2, temp + step * func(x2(i - 1), y2(i - 1)) ];
end

plot(x1, y1, x2, y2)
axis auto