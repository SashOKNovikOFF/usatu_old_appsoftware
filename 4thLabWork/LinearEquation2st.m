clear
clc

% Changed left up and right down elements
matrix = [-5,  -1, -1; ...
           0,   4, -1; ...
           0,  -1,  4]
freeColumn = [0; 1; 0]

lastX    = [0; 0; 0];
currentX = [0; 0; 0];
epsilon  = 0.0000001;
temp     = 1;

while temp >= epsilon
    lastX = currentX;
    
    for i = 1:1:3
        currentX(i) = 0;
        
        for j = 1:1:(i - 1)
            currentX(i) = currentX(i) + matrix(i, j) * currentX(j);
        end;
        
        for j = (i + 1):1:3
            currentX(i) = currentX(i) + matrix(i, j) * lastX(j);
        end
        
        currentX(i) = (freeColumn(i) - currentX(i)) / matrix(i, i);
    end
    
    temp = max(abs(currentX - lastX));
end

vpa(currentX, 6)
vpa(matrix^(-1) * freeColumn, 6)