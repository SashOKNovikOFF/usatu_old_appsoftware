clear
clc

x = -5:0.01:5;
y = (x.^4 + 3) ./ x;

plot(x, y);