clear
clc
syms x

func1 = @(x) (x + 1) ./ (2 * x + 1).^ (1.0 / 3.0)
integral(func1, 0, 13)
analyticFunc1 = int((x + 1) ./ (2 * x + 1)^(1.0 ./ 3.0), 0, 13)

func2 = @(x) (cos(x) - cos(x).^3).^(0.5)
Pi = pi; % lazy to rewrite from Pi to pi :)
trapz([-Pi/2 -Pi/3 -Pi/4 -Pi/6 0 Pi/6 Pi/4 Pi/3 Pi/2], ... 
[func2(-Pi/2) func2(-Pi/3) func2(-Pi/4) func2(-Pi/6) func2(0) ...
 func2(Pi/6)  func2(Pi/4)  func2(Pi/3)  func2(Pi/2)])
integral(func2, -pi/2, pi/2)
analyticFunc2 = vpa(int((cos(x) - cos(x).^3).^(0.5), -pi/2, pi/2), 6)

func3 = @(x) exp(1).^(1./x) .* x.^(-2)
integral(func3, 1, 2)
analyticFunc3 = vpa(int(exp(1).^(1./x) * x.^(-2), 1, 2), 6)

func3 = @(x) exp(1).^(1./x) .* x.^(-2)
integral(func3, 1, 2)
analyticFunc3 = vpa(int(exp(1).^(1./x) * x.^(-2), 1, 2), 6)

func4 = @(x) x.^3 ./ (5 / 8 - x.^4).^(1.5)
integral(func4, 0.5, 3^(0.5)/2)
analyticFunc4 = int(x.^3 ./ (5 / 8 - x.^4).^(1.5), 0.5, 3^(0.5)/2)