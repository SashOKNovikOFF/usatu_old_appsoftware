clear
clc

analyticFunc = dsolve('Dy + y * tan(x) = cos(x)^2', 'y(pi/4) == 0.5', 'x')

x1 = (pi / 4):0.01:(pi / 4 + 1);
y1 = subs(analyticFunc, x1);

func = @(x, y) cos(x).^2 - y .* tan(x);

parts = 500;
leftBorder  = pi / 4;
rightBorder = pi / 4 + 1;
step = (rightBorder - leftBorder) / parts;

x2 = leftBorder : step : rightBorder;
y2 = [0.5];

for i = 2:numel(x2)
    temp = y2(i - 1);
    y2   = [ y2, temp + step * func(x2(i - 1), y2(i - 1)) ];
end

plot(x1, y1, 'o', x2, y2, 'r')
axis auto