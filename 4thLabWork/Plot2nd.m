clear
clc

t = -0.5:0.0001:0.5;
x = t + exp(-t);
y = 2 * t + exp(-2 * t);

plot(x, y);