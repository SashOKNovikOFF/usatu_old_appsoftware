clear
clc

analyticFunc = dsolve('2 * (4 * y^2 + 4 * y - x) = Dx', 'x(0) == 0', 'y')

x1 = 0:0.001:0.1;
y1 = subs(analyticFunc, x1)

func = @(y, x) 2 .* (4 .* y.^2 + 4 .* y - x);

parts = 500;
leftBorder  = 0;
rightBorder = 0.1;
step = (rightBorder - leftBorder) / parts;

x2 = leftBorder : step : rightBorder;
y2 = [0];

for i = 2:numel(x2)
    temp = y2(i - 1);
    y2   = [ y2, temp + step * func(x2(i - 1), y2(i - 1)) ];
end

plot(x1, y1, x2, y2)
axis auto